#The concepts used in this game are property of Andrew Hussie, author of the webcomic Homestuck.
from player import *
from enemies import *
from world import *
from items import *
import random
import time
import datetime
import sys
import os
import cPickle as pickle

def savegame(player, world, current_node):
	with open("savegame", "r+b") as f:
		pickle.dump([player, world, current_node, InitialNode.count, RandomNode.quest_bed, RandomNode.forward_exit], f)

def status_check(player):
	checker = player.status()
	if (checker != 0):
		print "\nDo you want to (D)escribe, (U)se, or (R)emove an item? [Or (N)one?])"
		print "\nReminder! You are using the",player.sylladex_deck.description
		print "\nRemoving an item DESTROYS it forever. And ever. And Ever. And EVER."
		option2 = ""
		while (option2!="U" and option2!="N" and option2!="D" and option2!="R"):
			option2 = raw_input(">")
	
		if option2=="U":
			player.sylladex_deck.use_item(player)
			
		elif option2 =="D":
			player.sylladex_deck.desc_item()
			
		elif option2 == "R":
			player.sylladex_deck.remove_item()
			
		else:
			return

def menu(player, world, current_node):
	os.system('cls' if os.name == 'nt' else 'clear')
	print "Main Menu Options:"
	print "1) Status Check"
	print "2) Save Game"
	print "3) Exit Menu"
	
	option = ""
	while (option != "1" and option !="2" and option != "3"):
		option = raw_input(">")
	
	if (option == "1"):
		status_check(player)
		
	elif (option == "2"):
		savegame(player, world, current_node)
		
	elif (option == "3"):
		return
		
	raw_input("> Press [ENTER] to continue.")
		

def aggrieve(player, enemy):
	chance = random.randrange(0,15)*player.echeladdar
	print "You attack with your", player.weapon.name
	if ((enemy.speed-player.speed) < chance):
		damage = player.weapon.damage*player.echeladdar
		enemy.hitpoints -= damage
		if damage > enemy.hitpoints/5:
			print "You did massive damage!"
		 
		else:
			print "You damaged the enemy."
			
		print damage,"points of damage."
		
	else:
		print "You missed!"


		
def abscond(player, enemy):
	print "You attempt to ABSCOND!"
	if enemy.name=="The Black King":
		print "You can't escape from THE BLACK KING"
		return 0
		
	elif (player.speed - enemy.speed < random.randrange(0,6)*player.echeladdar):
		return 1
	
	else:
		return 0
		
def damage_player(player, enemy):
	chance = random.randrange(0,15)*player.echeladdar
	print "Your opponent lunges at you!"
	if (player.speed - enemy.speed < chance):
		damage =  enemy.damage
		player.adjustHP(damage)
		
		if (damage > player.hitpoints_max/5):
			print "You took massive damage!"
		
		else:
			print "You took damage."
			
		print damage,"points of damage."
		
	else:
		print "You avoided the attack!"
	
def god_tier_init(player):
	print "You approach the SLAB. There are four pillars on each corner of it."
	time.sleep(3)
	print "The SLAB is a dark red, with a light red cog symbol in the middle."
	time.sleep(2)
	print "You feel weary. Perhaps you should take a rest on this QUEST BED? (Y/N)"
	option = ""
	while (option!="Y" and option !="N"):
		option = raw_input(">")
	if option=="N":
		return
	else:
		print "You lie down on the SLAB that you just oddly called a QUEST BED."
		time.sleep(2)
		print "As you close your eyes, you feel a sharp pain in your chest."
		time.sleep(2)
		print "One of the DERSITE WARRIORS have found you and stabbed you. In the chest."
		print "Obviously."
		time.sleep(2)
		print "As you drift into unconsciousness, you are enveloped by a warm energy."
		time.sleep(2)
		print "Your body rises and explodes with light."
		time.sleep(2)
		print "As you look down, the DERSITE WARRIOR has fled."
		time.sleep(2)
		print "Furthermore, you're now wearing red pajamas."
		time.sleep(2)
		raw_input("> Press [ENTER] to continue.")
		os.system('cls' if os.name == 'nt' else 'clear')
		print "You're reached GOD TIER as a fully realized KNIGHT OF TIME."
		player.god_tier = 1
		player.description = "You're a maroon cape wearing, red pajama touting, fully realized KNIGHT OF TIME."
		player.hitpoints_max *= 4
		player.speed += 4
		player.hitpoints = player.hitpoints_max
		raw_input("> Press [ENTER] to continue.")
	
		
def strife(player, enemy, curr_cell):
	os.system('cls' if os.name == 'nt' else 'clear')
	print "STRIFE!"
	time.sleep(1)
	enemy.level_scaling(player)
	while (enemy.hitpoints > 0 and player.hitpoints > 0):
		os.system('cls' if os.name == 'nt' else 'clear')
		print "Your opponent is ", enemy.name, ".\n"
		enemy.printDescription()
		print "\n\n You are equipped with your", player.weapon.name
		if (player.echeladdar*player.weapon.damage*5 < enemy.hitpoints):
			print "You feel under-equipped for this strife!"
		
		else:
			print "You feel mostly confident against this enemy\n"
			
		print "Your Vita-Gel bar has", player.hitpoints,"/",player.hitpoints_max,"units left!"
		print "You sense that the enemy has", enemy.hitpoints,"units remaining."
		
		print """You can...
1) AGGRIEVE
2) ABSCOND
3) Check your status
		
		"""
		if (player.sylladex_deck.used+1>=player.sylladex_deck.capacity and player.sylladex=="1" and curr_cell.item != 0):
			print "You see an item on the ground:",curr_cell.item.name,"you can (C)APTCHALOGUE it."
			print "C) CAPTCHALOGUE"
		
		option = ""
		while (option != "1" and option != "2" and option != "3" and option != "C"):
			option = raw_input(">")
			
		if option == "1":
			aggrieve(player, enemy)
		
		elif (option == "2"):
			if (abscond(player, enemy)==1):
				print "You managed to ABSCOND!"
				break
			
			else:
				print "You failed. Miserably."
				
		elif (option =="3"):
			status_check(player)
			
		
		elif (option=="C" and curr_cell.item != 0 and player.sylladex_deck.used+1>=player.sylladex_deck.capacity and player.sylladex=="1"):
			throw = player.sylladex_deck.add_item(curr_cell.item)
			print "You CAPTHALOGUE the item!"
			print "Your",throw.name,"ejects from your SYLLADEX and hits the enemy!"
			enemy.hitpoints -= throw.damage
			print "You inflicted",throw.damage,"points of damage!"
			curr_cell.item = throw
			
		
		if (enemy.hitpoints>0 and player.hitpoints):
			damage_player(player, enemy)
		
		if (player.hitpoints<=0):
			print "You have died."
			
			if (player.god_tier==1):
				print "A faraway clock tolls."
				if random.randrange(0,2):
					player.hitpoints = player.hitpoints.max
					print "Your death was not JUSTIFIED, this time."
					print "You have been restored to full strength."
				else:
					print "Your death was HEROIC."
					sys.exit()
			
			else:
				sys.exit()
			
		raw_input("> Press ENTER to continue.")
	if (enemy.hitpoints ==0):
		player.exp_distrib(enemy)
	print "Strife ended."
	raw_input("> Press ENTER to continue.")
	
def world_movement(player, current_node):
	while (True):
		os.system('cls' if os.name == 'nt' else 'clear')
		
		if current_node.contains_enemy == 1:
			strife(player, current_node.enemy, current_node)
			current_node.contains_enemy = 0
			os.system('cls' if os.name == 'nt' else 'clear')
		
		if (current_node.quest_bed == 1 and player.god_tier==0):
			print "A red STONE SLAB is visible in the distance. You can investigate."
			print "I) INVESTIGATE"
		
		if (current_node.gate_mark == 1):
			print "\nYou've found a GATE to the next world!"
			print "G) Enter the Next World"
			
				
		if (current_node.item != 0):
			print "\nThere is a", current_node.item.name,"here!"
			print "C) CAPTCHALOGUE the item."
			
		print "\nHere you can..."
	
		if (current_node.forward_mark == 1):
			print "F) Go Forward"
		
		if (current_node.left_mark == 1):
			print "L) Go Left"
		
		if (current_node.right_mark == 1):
			print "R) Go Right"
		
		print "B) Go Back"
		
		print "M) Main Menu"
		
		option = raw_input(">")
		while (option != "F" and option!="I" and option != "L" and option != "R" and option != "G" and option != "B" and option != "M" and option != "C"):
				option = raw_input(">")
	
		old_node = current_node
		
		if option=="F" and current_node.forward_mark==1:
			current_node = current_node.forward
			
		elif option == "L" and current_node.left_mark==1:
			current_node = current_node.left
			
		elif option == "R" and current_node.right_mark==1:
			current_node = current_node.right
			
		elif option == "G" and current_node.gate_mark==1:
			if (InitialNode.count<4):
				current_node = current_node.gate
				
			else:
				return #Boss Time
		
		elif option == "I" and current_node.quest_bed==1:
			god_tier_init(player)
		
		elif option == "B":
			current_node = current_node.prev_node
			
		elif option == "C":
			space = player.sylladex_deck.add_item(current_node.item)
			if (space != 1):
				print "You CAPTCHALOGUED the item and your",player.sylladex_deck.description,"spat out your",space.name
				current_node.item = space
				
			else:
				print "You CAPTCHALOGUED the item!"
				current_node.item = 0
			
			raw_input("> Press [ENTER] to continue.")
			
			
			
		elif option == "M":
			menu(player, world, current_node)
		
		else:
			print "That wasn't an option. Try again"
			raw_input("Press ENTER to retry")
			
		if current_node.exist == 0:
			current_node.activate_node(old_node.dist, old_node)
				



def initSBURB():
		os.system('cls' if os.name == 'nt' else 'clear')
		print "SBURB version 0.0.1\n"
		time.sleep(2)
		print "(c) SKAIANET SYSTEMS INCORPORATED. ALL RIGHTS RESERVED.\n"
		time.sleep(2)
		print "SBURB client is running.\n"
		time.sleep(2)
		print "A local SBURB host user is attempting to connect with you.\n"
		time.sleep(2)
		print "Client has established connection with host.\n"
		time.sleep(2)
		raw_input("Press [ENTER] when ready.\n\n>")
		os.system('cls' if os.name == 'nt' else 'clear')
		print """
			':okkkkkkkkkxl;.			
		.;okOxxdkxdlldxdxxxOxl,.		
2	   ,dOdkkl, .;l:'':l:. 'lkkdko,		 
	;xkOxdo;,cxolc;..::loxc,:ddxOkx;	
  .dOkl:kl	'x,	 .xddo	 ,x'  lk:dOOd.	
 ,kkO;.ckc;:ko;::x:	 ld::;lk:;ckc.:Okk, 
'kkkko:dc  .k,	..	  '.  'k.  odldkkkk'
xkoxo  ld;:,lc			  ld;;'x: .dxdkx
kOkxx.;dxo					 .dxo,.xxOkk
Okl dkc	 :x;				;d: .lkd okk
kkd.xxo..ld'				,ol..dkd.dOk
kkOkd ,dko.					 .dkd'.xxkkk
dkoxd. lo.,;xl			  ok;,.dc 'xxdkx
'kkkkloxl  .k,	',	  ,'  ,O.  lxookkkx.
 ,kkkc :kc,,ko;:,dl	 ld;:;dk:;lO:.cOkx. 
  .dOOxcko	.x:	 .dkxd.	 :x.  ok:xkOo.	
	,dkOxodc,:oodl,..,lood:,cdoxkko,	
	  .lkxxOd;..;dl;;ld,..;dOkxxc.		
		 'cdkkxkkOxlcdkxkkxko:'			
			.;oxkkkkkkkOdc,		  
		"""
		print "Transforming Soffits",
		time.sleep(2)
		print "\rLaunching Manifestation Systems",
		time.sleep(2)
		print "\rReorganizing Keys"
		print "[",
		for x in range(0,15):
			print "|",
			time.sleep(1)
		print "]"
 

def startgame(player):
		os.system('cls' if os.name == 'nt' else 'clear')
		today = datetime.date.today() #Useful for save creation
		month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		player.setgender(raw_input("What is your gender? [F or M])\n>"))
		print "A young ",player.pronoun[2]," stands in ",player.pronoun[1]," bedroom. It just so happens that today, the ", today.day," of ", month[today.month-1], "is this young",player.pronoun[2],"'s birthday. Though it was thirteen years ago ",player.pronoun[0]," was given life, it is only today ",player.pronoun[0],"will be given a name!\nWhat will ",player.pronoun[1]," name be?" 
		player.birthday = "%s %i" % (month[today.month-1], today.day)
		player.setname(raw_input())
		os.system('cls' if os.name == 'nt' else 'clear')
		player.name = player.name.upper()
		print "Your name is ",player.name, " you have no interests except staring at your CLOCK, using your COMPUTER, and sleeping. As mentioned before, it is your BIRTHDAY. Your DAD got you a beta copy of a game you have been anticipating, called SBURB.\n\n"
		time.sleep(1)
		print "Your computer glows with an orange light into your dark, uncomfortably plush room. It beeps incessantly. One of your CHUMS is messaging you!"
		raw_input("> Press [ENTER] to read pesterlog.") 
		os.system('cls' if os.name == 'nt' else 'clear')
		print "[___________________PESTERCHUM V3.14___________________]"
		print "significantlyDelicious (sD) started pestering confusedRabbit (cR)"
		time.sleep(1)
		print "sD: h33y dude happ33h birthday!! :33"
		time.sleep(1)
		print "cR: Thanks Geo."
		time.sleep(1)
		print "sD: did you g33t th33 gam33 y33t? i'v33 b3333n waiting for you!"
		time.sleep(1)
		print "cR: Dad dropped it off this morning through the garbage chute."
		time.sleep(1)
		print "sD: w33ll hurry up! w33'r33 waiting for you to host the s33rver!"
		time.sleep(1)
		print "significantlyDelicious (sD) ceased pestering confusedRabbit (cr)"
		print "\n\n Your friend may have left rudely, but has a point. You've been wanting to play this game for a while. Perhaps you should pop the disc in?\n"
		raw_input("Press [ENTER] to continue")

		
def game_start_proper(player):
	os.system('cls' if os.name == 'nt' else 'clear')
	print "The Earth beneath you begins to rumble."
	time.sleep(4)
	print "You hear what sounds like airplanes passing overhead."
	time.sleep(4)
	print "You look outside and see your entire town up in flames."
	time.sleep(4)
	print "Meteors are passing overhead, not airplanes."
	time.sleep(4)
	print "You see your DAD's car outside, caught in-between two untouched patches of land."
	time.sleep(4)
	print "You can't tell if he's actually inside."
	time.sleep(4)
	print "You don't know how long you've been staring out your window."
	time.sleep(4)
	print "You realize your computer has been blinking for a while now, you return to it."
	raw_input("> Press [ENTER] to read pesterlog")
	print "[___________________PESTERCHUM V3.14___________________]"
	print "awkwardTyrant (aT) started pestering confusedRabbit (cR)"
	print "aT: the entire worlds in flames dude"
	print "aT: all of it"
	print "aT: hell are you even alive?"
	print "aT: yeah well sD's been getting frantic on deploying all the game shit in your house"
	time.sleep(2)
	print "aT: we did this man"
	time.sleep(4)
	print "cR: What did we do?"
	time.sleep(4)
	print "aT: we started the game. dude the game's a frign trap it started this"
	time.sleep(4)
	print "cR: It's just a game, Tony."
	time.sleep(4)
	print "awkwardTyrant (aT) has lost the connection with PESTERCHUM servers. Error 413"
	
	time.sleep(4)
	print "\n\nYou're sure it's just a game. Tony's just going mad."
	time.sleep(2)
	print "Then again, you just saw meteors crashing into the Earth."
	time.sleep(2)
	print "What was that about deploying game things in your house?"
	raw_input("> Press [ENTER] to continue.")
	os.system('cls' if os.name == 'nt' else 'clear')
	
	print "You heard a loud crash in your house, sounds like the glass table in the living room."
	time.sleep(4)
	print "A crystal orb flies through your door and shatters on contact with your computer desk."
	time.sleep(4)
	print "You look at the fragments of the orb, confused. What the hell is this?"
	time.sleep(4)
	print "As you go to reach for a fragment, the floor beneath you shakes again."
	time.sleep(4)
	print "An intense light blinds you momentarily."
	time.sleep(4)
	raw_input("> Press [ENTER] to continue.")
	os.system('cls' if os.name == 'nt' else 'clear')
	print "You're suddenly on your floor, your house is suddenly not where it belongs. It's dark out."
	time.sleep(4)
	print "A glowing black orb appears suddenly, pulsating. You have a strange desire to throw something into it."
	time.sleep(4)
	print "KERNELSPRITE: That would be a bad idea."
	time.sleep(4)
	print player.name,": Who said that?"
	time.sleep(3)
	print """KERNELSPRITE: I did. Me. Your KERNELSPRITE. 
The glowing orb thing."""
	time.sleep(4)
	print player.name,": Oooh.."
	time.sleep(2)
	print """KERNELSPRITE: I'm your guide for the game you're playing. You've entered
entered into what we call a glitched session."""
	time.sleep(6)
	print player.name,": Glitched Session?"
	time.sleep(2)
	print """KERNELSPRITE: The goal of the game you're playing is to create a new
universe. A real one, which happens first by destroying your world.
Now don't feel bad about that, it's inevitable. Someone was going to play sooner or later.
	
Your particular session is 'broken', glitched. For one, I shouldn't be able to
communicate with you in a meaningful way until you prototype me. Clearly 
that wasn't the case."""
	time.sleep(6)
	print player.name,": ~you have no idea what's going on~"
	time.sleep(6)
	print """KERNELSPRITE: ...But that's beside the point. Your session is a cross between a standard
and NULL session. You are unable to create a new universe. You're also the only player here.
This game isn't meant for one person."""
	time.sleep(6)
	print """KERNELSPRITE: However, I mean that literally. There's a subversion of this game that's supposed
to kick in when a single player attempts to do play. You're caught in-between."""
	time.sleep(1)
	print player.name,": But my friends were going to play with me!"
	time.sleep(2)
	print "KERNELSPRITE: Oh, yeah. They died. Sorry about that."
	time.sleep(4)
	print """KERNELSPRITE: Well, if I'm right you're about to be thrown onto your starting world.
Now due to the nature of this session, we're going to be cut off from each other when this happens.
Pay attention this this next part VERY. CAREFULLY."""
	print "---------Now really, if you've been skipping the story this is the part you actually NEED--------"
	time.sleep(6)
	raw_input("> Press [ENTER] to continue.")
	os.system('cls' if os.name == 'nt' else 'clear')
	print """KERNELSPRITE: You're going to go have to explore four or five worlds. Find the
gates to each subsequent world by travelling wherever you are. Along the way you'll
find various creatures that the game has put out at you. Be ready for anything.
While I'm not sure, I do believe that your "final boss" is the Black King of Derse."""
	time.sleep(6)
	print """\nKERNELSPRITE: Now, let's make sure you're prepared. I see you haven't allocated your 
STRIFE SPECIBUS yet, let's do that."""
	raw_input("> Press [ENTER] to continue.")
	os.system('cls' if os.name == 'nt' else 'clear')
	print "The only useful things you can see here to set your STRIFE SPECIBUS to are your (H)AMMER and (K)ATANA"
	print "Which do you choose?"
	option = ""
	while (option!="H" and option!="K"):
		option = raw_input(">")
	
	if option =="H":
		player.strifespec.init_specibus(Hammer())
		
	else:
		player.strifespec.init_specibus(Sword())
		
	print "Your STRIFE SPECIBUS has been set to ",player.strifespec.description
	time.sleep(1)
	print "Your KERNELSPRITE seems to be throwing a tantrum. It's throwing things into your Sylladex rather quickly."
	player.sylladex_deck.add_item(Painting())
	player.sylladex_deck.add_item(Nails())
	player.sylladex_deck.add_item(Hammer())
	player.sylladex_deck.add_item(Sword())
	
	time.sleep(2)
	print "Everything flashes like the TV static of old."
	time.sleep(4)
	print "You feel disoriented."
	time.sleep(4)
	print "As you shake it off, you find yourself in an unfamiliar place."
	time.sleep(4)
	print player.name,": I guess it's time, then."
	raw_input("> Press [ENTER] to begin.")
	
	
		
def window_jump_initial(player):
		a = random.randrange(0,10)
		if (a < 8):
				print "You jumped out of the window and faceplanted into the ground. What's wrong with you?"
				player.adjustHP(10)
				if player.hitpoints <= 0:
						print "You died. Totally your fault."
						sys.exit()
				else: 
						"You promptly pick yourself up and limp back to your room."
		else: 
				print "You jump out of your window and somersault to safety. You clearly look like a badass"		
		print "You then return to your room."
def sylladex_init(player):
		os.system('cls' if os.name == 'nt' else 'clear')
		print """You notice your FETCH MODUS has not been set yet. One of your few birthday presents
included two new MODI, the STACK and the ARRAY.

The STACK MODUS only allows you to select the last item entered into it. However, if you attempt to enter another
item while it is at its max capacity, the first item will be hurled out of it as a projectile.

The ARRAY MODUS allows you to select any item without issue. However, it can not be weaponized.

Which of the MODI do you choose?
1) STACK MODUS (WEAPONIZABLE)
2) ARRAY MODUS (BOOOOOOOORING)
"""
		option = ""
		while (option!="1" and option!="2"):
				option = raw_input(">")
				player.sylladex=option
				player.init_sylladex()
		
		roomopt(player)
		
			
def roomopt(player):
		if player.sylladex=="":
			sylladex_init(player)
		os.system('cls' if os.name == 'nt' else 'clear')
		print """Your room is dark and small, but comfortable. Your bed is in the
corner, and on it is a PAINTING that you planned to hang up. The floor has a HAMMER and NAILS resting on it,
most likely for that painting you didn't bother with. Next to your computer you have your trusty
LACKLUSTER KATANA, which you bought at a recent convention for $413.
"""

def init_rgame(player):
		option = ""
		os.system('cls' if os.name == 'nt' else 'clear')
		while (True):
				
				print """You can... 
1) Pop in the SBURB disc and start playing with your friends.
2) Look around your room.
3) Jump out the window.

What will you do?
"""

				option = raw_input(">")
				if option=="1":
					if player.sylladex=="":
						print "You're sure you should be doing something else first..."
					
					else:
						initSBURB()
						break
				if option=="2":
						roomopt(player)
						raw_input("> Press [ENTER] to continue.")
						os.system('cls' if os.name == 'nt' else 'clear')
				if option=="3":
						window_jump_initial(player)
			
player = Player()			
def final_boss(player):
	os.system('cls' if os.name == 'nt' else 'clear')
	current_node = FinalNode()
	enemy = BlackKing()
	print "You land on a massive chessboard world."
	time.sleep(2)
	print "You... actually don't know why."
	time.sleep(3)
	print "And then it hits you."
	time.sleep(4)
	print "The Black King is here. Literally. In. Your. Face."
	strife(player, enemy, current_node)

def credits(player):
	os.system('cls' if os.name == 'nt' else 'clear')
	print "You've defeated The Black King."
	time.sleep(2)
	print "You feel content with avenging your planet."
	time.sleep(2)
	print "Even though it was entirely your fault it got destroyed."
	time.sleep(2)
	print "Sort of."
	time.sleep(2)
	if (player.god_tier ==1):
		print "At least you can finally rest, young god."
	else:
		print "At least you can finally relax."
	time.sleep(4)
	print "\n\n\n\nEND."
	raw_input("> Press [ENTER] to end.")
						
savefile = open("savegame", "a+")
savefile.seek(0)
savefilecheck = savefile.read(1)
os.system('cls' if os.name == 'nt' else 'clear')
print "		}_____SBURB PYTHON_____{"
print "Do you want to start a (N)EW GAME or (C)ONTINUE your adventure?"
option = ""
while (option!= "N" and option != "C"):
	option = raw_input(">")
player = Player() #We need a player.
if (not savefilecheck) or option=="N":
		startgame(player)
		init_rgame(player)
		game_start_proper(player)
		world = World()
		start_node = World.map[0]
		world_movement(player, start_node)
		final_boss(player)
		credits(player)
		
else:
	with open("savegame", "r+b") as f:
		savedata = pickle.load(f)
		player = savedata[0]
		player.sylladex_deck.print_deck()
		world = savedata[1]
		current_node = savedata[2]
		InitialNode.count = savedata[3]
		RandomNode.quest_bed = savedata[4]
		RandomNode.forward_exit = savedata[5]
		world_movement(player, current_node)
		final_boss(player)
		credits(player)
		