Based on Andrew Hussie's webcomic, "Homestuck."
Available for reading at http://mspaintadventures.com/
Concepts are not my intellectual property.


[_________________SBURB PYTHON V3 README__________________]
Run SBURB.exe/.py (Depending on your distribution)

If you've been mashing ENTER and random commands at first, you probably ended up with the STACK MODUS.

The ARRAY MODUS is much easier to use. I recommend it to newbies.
The STACK MODUS works like the stack data structure.

The object you put in last is the object you can interact with.
For example:

[ DINGIT | DANGIT | DURGIT ]
     0       1         2
	 
If you add an item to your sylladex, you'd get

[DINGIT | DANGIT | DIRGIT | ITEM ]
   0         1       2       3
   
You can only USE or REMOVE the last item now, being index 3.
To interact with the other items, remove until you get the item you need.

Describe works with any index.


		IF YOU'RE NOT SURE WHAT AN ITEM DOES, TRY TO (D)ESCRIBE IT!

-------------------------------------------------------