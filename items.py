import random

#Generic item classification
class Item():
	def __init__(self):
		self.damage = 1
		self.name = "Perfectly Generic Object"
		self.description = "Perfectly Generic Object"
		self.strifey = 0 #For checks if we can allocate to the strife specibus
		self.worth = 0
		
	def use(self, player):
		return 0

class CCard(Item):
	def __init__(self):
		Item.__init__(self)
		self.name = "CAPTCHALOGUE CARD"
		self.description = "One of the cards used in your Sylladex."
		
	def use(self, player):
		player.sylladex_deck.add_card()
		print "SYLLADEX expanded by one card!"
		return 1
		
class VitaGel(Item):
	def __init__(self):
		Item.__init__(self)
		self.name = "VITA-GEL"
		self.description = "One cube of odd blue stuff. You think it restores health."
		
	def use(self, player):
		player.adjustHP(-(player.hitpoints_max/3))
		if player.hitpoints>player.hitpoints_max:
			player.hitpoints = player.hitpoints_max
		return 1
		
		
class Arms(Item):
	def __init__(self):
		Item.__init__(self)
		self.damage = 1
		self.name = "FAKE ARMS"
		self.description = "Meant for HILARIOUS ANTICS"
		self.worth = 0
	
	def use(self, player):
		player.weapon = self
		return 1
	
class Nails(Item):
	def __init__(self):
		Item.__init__(self)
		self.name = "NAILS"
		self.damage = 6
		self.weight = 0
		self.description = "Some iron nails."
		self.worth = 2

class Painting(Item):
	def __init__(self):
		Item.__init__(self)
		self.damage = 10
		self.name = "PAINTING"
		self.description = """A painting with an extremely thick canvas. It'll most likely
last one launch."""
		self.worth = 10

	
class Hammer(Item):
	def __init__(self):
		Item.__init__(self)
		self.damage = 10
		self.weight = 2
		self.strifey = 1
		self.name = "HAMMER"
		self.description = "A brand name Hammer. Wooden handle, iron head, you know what this is."
		self.specibus = "Hammerkind"
		self.worth = 5
	
	def use(self, player):
		if player.strifespec.description=="An unallocated Strife Specibus.":
			player.strifespec.init_specibus(self)
	
		if player.strifespec.description == self.specibus:
			player.weapon = self
			return 2
		else:
			return 0
	
class Zillyhoo(Hammer):
	def __init__(self):
		Hammer.__init__(self)
		self.description = """The legendary Zillium warhammer, the Warhammer of ZIllyhoo. 
		There she is. Forged in fire by the smiths of Pipplemop, commissioned 
		by the sage Lord of the Wozzinjay Fiefdom in the Realm of the Snargly Fruzmigbubbin"""
		self.damage = 100
		self.name = "WARHAMMER OF ZILLYHOO"
		self.weight = 5
		self.worth = 10000000 #It's not a zillion, but it's a start.
		
class Sledgehammer(Hammer):
	def __init__(self):
		Hammer.__init__(self)
		description = "A sledgehammer. Wooden handle, iron head, incredibly heavy."
		self.name = "SLEDGEHAMMER"
		self.weight = 10
		self.worth = 6
		
	
class Sword(Item):
	def __init__(self):
		self.damage = 10
		self.weight = 15
		self.strifey = 1
		self.worth = 5
		self.specibus = "Bladekind"
		self.name = "LACKLUSTER KATANA"
		self.description = "A cheap Katana, most likely bought at a convention center."
		
	def use(self, player):
		if player.strifespec.description=="An unallocated Strife Specibus.":
			player.strifespec.init_specibus(self)
			
		if player.strifespec.description == self.specibus:
			player.weapon = self
			return 2
		else:
			return 0
		
class Caledfwlch(Sword):
	def __init__(self):
		Sword.__init__(self)
		self.damage = 40
		self.weight = 20
		self.worth = 50
		self.name = "CALEDFWLCH"
		self.description = "A powerful sword. It seems to have had a similarly powerful previous owner."
		
class Sord(Sword):
	def __init__(self):
		Sword.__init__(self)
		self.damage = -1
		self.name = "sord. . . . ."
		self.description = "This nearly 2D \"sword\" is so unbelievably shitty that you can barely get a grip on it."

class Zillywair(Sword):
	def __init__(self):
		Sword.__init__(self)
		self.damage = 100
		self.name = "CUTLASS OF ZILLYWAIR"
		self.description = """The legendary Zillium sword. 
		Swashed from the buckles of the rough'n tumble Bellyjape Seamen 
		and offered atop the kingdom's last known wildly 
		occurring pluffy dimplepillow to the resplendent 1st 
		Rumbylumplewiffig of the Horsehorsehorse Administration"""
		worth = 10000000
		

class ItemGroup():
	def pull(self):
		self.x = random.randrange(0,99)
		
		if (self.x < 85): #Common items
			self.list = {0:VitaGel(),1:Nails(),2:Sord(),3:Hammer(),4:Sword()}
			return self.list[random.randrange(0,5)]
			
		elif (self.x < 95): #Uncommon items
			self.list = {0:CCard(),1:Sledgehammer(),2:Caledfwlch(),3:Painting()}
			return self.list[random.randrange(0,4)]
			
		elif (self.x < 99): #Rare items
			self.list = {0:Zillywair(),1:Zillyhoo()}
			return self.list[random.randrange(0,2)]
			
class Meteor(Item):
	def __init__(self):
		Item.__init__(self)
		self.name = "METEOR"
		self.description = "A massive meteor that impacted Skaia. You think it could inflict massive damage... if properly delivered."
		self.damage = 500
		