import random
from items import *
from enemies import *

class InitialNode():
	count = 0
	def __init__(self):
		InitialNode.count += 1
		self.dist = 0
		self.forward_mark = 1
		self.left_mark = 1
		self.right_mark = 1
		self.gate_mark = 0
		self.quest_bed = 0
		self.prev_node = self
		self.forward = RandomNode(self.dist, self)
		self.left = RandomNode(self.dist, self)
		self.right = RandomNode(self.dist, self)
		self.exist = 1
		self.item = 0
		self.contains_enemy = 0
		self.description = """You are at the first step of a long journey forward.
The path breaks into three different routes.
"""

class FinalNode(InitialNode):
	def __init__(self):
		InitialNode.__init__(self)
		self.item = Meteor()

class RandomNode(InitialNode):
	forward_exit = 0
	quest_bed = 0
	def __init__(self, dist, prev_node):
		RandomNode.forward_exit = 0
		self.dist = dist+1
		self.exist = 0
		self.forward_mark = 0
		self.left_mark = 0
		self.right_mark = 0
		self.item = 0
		self.quest_bed = 0
		self.gate_mark = 0
		self.contains_enemy = 0
	def activate_node(self, dist, prev_node):
		if (random.randrange(0,2)):
			self.itemg = ItemGroup()
			self.item = self.itemg.pull()
	
		if (random.randrange(0,5)<3):
			self.contains_enemy = 1
			self.enemy_lister = EnemyGroup()
			self.enemy = self.enemy_lister.pull()
			
		self.exist += 1
		if (dist >= 5):
			if (RandomNode.forward_exit == 0 and InitialNode.count < 4):
				self.gate_mark = 1
				RandomNode.forward_exit = 1
				self.gate = InitialNode()
				
				
			elif (RandomNode.forward_exit == 1 and RandomNode.quest_bed == 0 and self.quest_bed == 0):
				RandomNode.quest_bed = 1
				self.quest_bed = 1
				
		
		else:
			if (random.randrange(0,2)):
				self.forward = RandomNode(self.dist, self)
				self.forward_mark = 1
		
			if (random.randrange(0,2)):
				self.left = RandomNode(self.dist, self)
				self.left_mark = 1
			
			if (random.randrange(0,2)):
				self.right = RandomNode(self.dist, self)
				self.right_mark = 1
			
		self.prev_node = prev_node
		
class World():
	map = []
	map.append(InitialNode())

		
	
