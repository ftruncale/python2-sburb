from sylladex import *
from items import *

class Player():
	prototyped = 0
	def __init__(self):
		self.hitpoints = 100
		self.speed = 10
		self.name = "NULL"
		self.hitpoints_max = 100
		self.echeladdar = 1
		self.sylladex = ""
		self.exp = 0
		self.exp_required = 1
		self.weapon = Arms()
		self.strifespec = StrifeSpecibus()
		self.god_tier = 0
		self.birthday = "Null nil"
		self.pronoun = ["it", "its", "it"]
		self.description = "You are a scrawny teenager."
	
	def adjustHP(self, adjustment):
		self.hitpoints -= adjustment
		
	def directHP(self, new):
		self.hitpoints = new
		
	def init_sylladex(self):
		if self.sylladex=="1":
			self.sylladex_deck = StackModus()
		
		if self.sylladex=="2":
			self.sylladex_deck = ArrayModus()

	def setgender(self, newgender):
		self.gender = newgender
		self.init_pronoun()

	def init_pronoun(self):
		if self.gender=="M":
			self.pronoun = ["he", "his", "man"]
		if self.gender=="F":
			self.pronoun = ["she", "her", "woman"]


	def setname(self, newname):
		self.name = newname

	
	def exp_distrib(self, enemy):
		self.exp += enemy.exp
		while (self.exp >= self.exp_required):
			self.echeladdar += 1
			print "You have moved a rung up your ECHELADDAR!"
			print "You feel tougher, faster, and thirst for Faygo."
			self.hitpoints_max *= 1.5
			self.hitpoints = self.hitpoints_max
			self.speed *= 1.2
			self.exp_required *= 3
			self.exp = 0
			
	def status(self):
		print "Your name is", self.name
		print "You were born on", self.birthday,"."
		print self.description
		print "You have the",self.sylladex_deck.description,"set as your SYLLADEX MODUS."
		print "You prefer the",self.strifespec.description,"specibus."
		print "You are equipped with your",self.weapon.name,"."
		print "You are on rung number",self.echeladdar,"of your ECHELADDAR!"
		print "Your VITA-GEL BAR is at",self.hitpoints,"/",self.hitpoints_max
		if len(self.sylladex_deck.deck)!=0:
			print "\n\n Your SYLLADEX contains..."
			self.sylladex_deck.print_deck()
			return 1
			
		return 0
		