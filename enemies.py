from player import *

class Enemy():
	def __init__(self):
		self.hitpoints = 20
		self.damage = 2
		self.description = ""
		self.speed = 1
		self.exp = 1
	
	def printDescription(self):
		print self.description
		if Player.prototyped==1:
			"It has been powered up due to your prototyping!"
		if Player.prototyped==2:
			"It has been weakened due to your prototyping!"
			
	def level_scaling(self, player):
		self.damage *= player.echeladdar/3
		self.hitpoints *= player.echeladdar*10
		self.speed *= player.echeladdar/2
			
		
class Imp(Enemy):
	def __init__(self):
		Enemy.__init__(self)
		self.name = "an imp"
		self.speed = 10
		self.description = "A small enemy armed with only their claws."
		self.exp = 3
		if Player.prototyped==1:
			self.hitpoints +=100
			self.damage += 20
		if Player.prototyped==2:
			self.hitpoints -=50
			self.damage -= 5

class Brute(Enemy):
	def __init__(self):
		Enemy.__init__(self)
		self.speed = 5
		self.name = "a brute"
		self.description = "A huge enemy equipped with ridiculous Brute strength."
		self.exp = 10
		self.damage +=30
		if Player.prototyped==1:
			self.hitpoints +=200
			self.damage += 100
		if Player.prototyped==2:
			self.hitpoints -= 50
			self.damage -= 60
			
class BlackKing(Enemy):
	def __init__(self):
		Enemy.__init__(self)
		self.speed = 20
		self.name = "The Black King"
		self.exp = 60
		self.hitpoints += 1000
		self.damage += 150
		self.description = "The ruler of Derse and your final boss. Defeat him and win your session!"
	

class EnemyGroup():
	def pull(self):
		self.list = {0:Imp(),1:Brute()}
		return self.list[random.randrange(0,2)]
