from items import *

class Sylladex():
	def __init__(self):
		self.capacity = 10
		self.used = 0
		self.deck = []
	
	def add_card(self):
		self.capacity += 1
		
	def add_item(self, item):
		if len(self.deck)+1 < self.capacity:
			self.deck.append(item)
			return 1
			
		else:
			return 0
	
	def print_deck(self):
		x = 0
		for card in self.deck:
			print x,")",card.name
			x+=1
			
	
	def use_item(self, player):
		self.option = 99999999
		while self.option > len(self.deck)-1:
			self.option = raw_input("Select item index:\n>")
			if self.option.isdigit():
				self.option = int(self.option)
		
		self.result = self.deck[self.option].use(player)
		if self.result == 1:
			self.deck.pop(self.option)
			self.used -= 1
		
		elif self.result == 2:
			print "Weapon equipped."
			self.used -= 1
		
		else:
			print "You can't use this item."
			
	def desc_item(self):
		self.option = 99999999
		while self.option > len(self.deck)-1:
			self.option = raw_input("Select item index:\n>")
			self.option = int(self.option)
		
		print self.deck[self.option].description 
		
class StackModus(Sylladex):
	def __init__(self):
		Sylladex.__init__(self)
		self.description = "STACK MODUS"
		
	def add_item(self, item):
		if self.used+1 == self.capacity:
			self.deck.append(item)
			return self.deck.pop(0)
		
		elif self.used+1 < self.capacity:
			self.deck.append(item)
			self.used += 1
			return 1
		
	def use_item(self, player):
		self.result = self.deck[len(self.deck)-1].use(player)
		if self.result == 1:
			self.deck.pop()
			self.used -= 1
			
		elif self.result == 2:
			print "Weapon equipped."
			self.used -= 1
		else:
			print "You can't use this item."
	
	def remove_item(self):
		return self.deck.pop()
		
class ArrayModus(Sylladex):
	def __init__(self):
		Sylladex.__init__(self)
		self.description = "ARRAY MODUS"
		
	def add_item(self, item):
		if self.used+1 < self.capacity:
			self.deck.append(item)
			self.used += 1
			return 1
			
		else:
			return 0

	def remove_item(self):
		self.option = 99999999
		
		self.option = raw_input("Select item index:\n>")
		if self.option.isdigit():
			self.option = int(self.option)
			
			if self.option <= len(self.deck)-1:
				self.deck.pop(self.option)
				self.used -= 1
			
			else:
				print "Invalid index."
		
		else:
			print "Invalid index."
		
class StrifeSpecibus():
	def __init__(self):
		self.description = "An unallocated Strife Specibus."
		self.deck = []
	
	def init_specibus(self, item):
		if (item.strifey == 1):
			self.description = item.specibus
			return 1
		
		else:
			return 0